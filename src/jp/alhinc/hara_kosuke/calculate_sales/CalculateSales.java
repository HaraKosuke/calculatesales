package jp.alhinc.hara_kosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(final String[] args) {
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
		BufferedReader br = null;

		if(!readFile(args[0], "branch.lst", "[0-9]{3}", "支店", branchNames,branchSales)) {
            return;
		}

		if(!readFile(args[0], "commodity.lst", "[A-Za-z0-9]{8}", "商品", commodityNames,commoditySales)) {
			return;
		}

		List<File> rcdFiles = new ArrayList<>();
		File[] files = new File(args[0]).listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for (int i = 0; i < rcdFiles.size(); i++) {
			List<String> branchInfo = new ArrayList<>();
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					branchInfo.add(line);
				}
				if (branchInfo.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				if (!branchNames.containsKey(branchInfo.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				if (!commodityNames.containsKey(branchInfo.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}
				if (!branchInfo.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				Long fileSale = Long.parseLong(branchInfo.get(2));
				Long saleAmount = branchSales.get(branchInfo.get(0)) + fileSale;

				Long saleCommodity = commoditySales.get(branchInfo.get(1)) + fileSale;
				if (saleAmount >= 10000000000L || saleCommodity >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(branchInfo.get(0), saleAmount);
				commoditySales.put(branchInfo.get(1), saleCommodity);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if (!writeFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		if (!writeFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	private static boolean readFile(String filePath, String filename, String patern, String category,
			Map<String, String> mapName, Map<String, Long> mapSale) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, filename);
			if (!file.exists()) {
				System.out.println(category + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if ((items.length != 2) || (!items[0].matches(patern))) {
					System.out.println(category + "定義ファイルのフォーマットが不正です");
					return false;
				}
				mapName.put(items[0], items[1]);
				mapSale.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean writeFile(String filePath, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales) {
		BufferedWriter bw = null;
		try {

			File file = new File(filePath, fileName);

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key : mapNames.keySet()) {

				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
